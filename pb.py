''' For phylogenies, see: https://paleobiodb.org/data1.2/taxa_doc.html.
For fossil occurrences, see: https://paleobiodb.org/data1.2/occs_doc.html

Make sure to review comments throughout the code for additional tips, and
use pythons help() for reminders when playing with it.
'''
__author__ = "Jared McArthur"
__copyright__ = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Jared McArthur"
__email__ = "jared.mcarthur@utexas.edu"
__status__ = "Development"

import requests as r
import json
import pprint as pp

''' these vars are for the generatePhylogeny() function
and are accessing the data as described here:
https://paleobiodb.org/data1.2/taxa_doc.html '''

phylo_url = 'https://paleobiodb.org/data1.2/taxa/list.json?name='
phylo_tax = 'tetrapoda'
phylo_opt = {'rel':'all_parents'} # Specify additional arguments in a dict


''' these vars are for the generateTaxaDict() function
and are accessing the data as described here:
https://paleobiodb.org/data1.2/occs_doc.html
'''

occ_url = 'https://paleobiodb.org/data1.2/occs/list.json?base_name='
occ_tax = 'stegosaurus'
# UPDATE INTERVAL BASED ON THE TAXA YOU ARE ANALYZING!
occ_opt = {'interval':'jurassic','show':'loc,class'} # Specify additional arguments in a dict
dictionary = {}

def pbHelp():
	''' Call this function for some helpful
		tips.
	'''
	print '''
generatePhylogeny requires a URL and taxon name to be supplied. You can call
this function (using some pre-defined values in the module) like:
	
	result = pb.generatePhylogeny(pb.phylo_url,'tetrapoda',**pb.phylo_opt)

You can also specify your own dictionary of options by using the
format:

	{'option1':'value','option2':'value'}

See the link in the module description for acceptable values.

Calling generateTaxaDict is similar (again using some pre-defined values):

	foo = pb.generateTaxaDict(pb.occ_url,'stegosaurus',**pb.occ_opt)

*** MAKE SURE TO UPDATE THE TIME PERIOD IN THESE OPTIONS!!! ***
You can update with pb.occ_opt['interval'] = 'someperiod'

To see what is in your dictionary (really just a hashmap):

	pb.printDict(foo)

Or, all in one go:

	pb.printDict(pb.generateTaxaDict(pb.occ_url,'stegosaurus',**pb.occ_opt))
'''

def generatePhylogeny(url,tax,**kwargs):
	''' This takes either the defaults defined
		in the source (see DATA section of help), 
		or custom parameters to return a
		phylogeny for the specified taxon
	'''
	print 'Generating phylogeny for ' + tax
	if kwargs is not None:
		url = url + tax
		for k,v in  kwargs.iteritems():
			url = url + '&' + k + '=' + v
	return r.get(url)

def generateTaxaDict(url,tax, **kwargs):
	'''	This queries a separate service of the API
		to generate a dictionary with the taxa as the
		keys, and the count of entries in the database
		as the values
	'''
	print 'Generating a frequency map of taxa for: ' + tax

	if kwargs is not None:
		url = url + tax
		for k,v in  kwargs.iteritems():
			url = url + '&' + k + '=' + v
	resp = r.get(url)
	dictionary = {}
	for x in resp.json()['records']:
		name = str(x['tna'])
		if dictionary.has_key(name):
			dictionary[name] += 1
		else:
			dictionary[name] = 1
	print 'Returning map'
	return dictionary

def printPhylogeny(response):
	''' Take the result of generatePhylogeny()
		and pass it here. It will print a simple
		list of all ancestral taxa down to the root
	'''
	for x in response.json()['records']:
		print x['nam']

def printDict(dictionary):
	''' Uses pretty print to output the
		dictionary in a nicer format than dumping
		raw JSON
	'''
	pp.pprint(dictionary, indent=2)

#########################################################
''' Don't try to run this as a script '''
if __name__ == '__main__':
	exit
